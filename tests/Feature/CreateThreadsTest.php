<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Activity;


class CreateThreadsTest extends TestCase
{
    

    public function test_guest_may_not_create_threads(){

        $this->get('/threads/create')
        ->assertRedirect('/login');

        $this->post('/threads')
        ->assertRedirect('/login');
    }



    public function test_an_authenticated_user_can_create_new_forum_threads()
    {
        
        $this->signIn();
        $thread = make('App\Models\Thread');
        $response = $this->post('threads',$thread->toArray());
        $this->get($response->headers->get('Location'))
        ->assertSee($thread->title)
        ->assertSee($thread->body);
    }

    public function test_a_thread_requires_a_title(){
        $this->publishThread(['title' => null])->assertSessionHasErrors('title');
    }

    public function test_a_thread_requires_a_body(){
        $this->publishThread(['body' => null])->assertSessionHasErrors('body');
    }

    public function test_a_thread_requires_a_valid_channel(){
        $this->publishThread(['channel_id' => null])->assertSessionHasErrors('channel_id');
        $this->publishThread(['channel_id' => 99999])->assertSessionHasErrors('channel_id');
    }

    public function test_unauthorized_users_may_not_delete_threads(){
        $thread = create('App\Models\Thread');
        $this->delete($thread->path())->assertRedirect('/login');

        $this->signIn();
        $this->delete($thread->path())->assertStatus(403);
    }
    public function test_authorized_users_can_delete_threads(){
        $this->signIn();
        $thread = create('App\Models\Thread',['user_id' => auth()->id()]);

        $reply = create('App\Models\Reply',['thread_id'=>$thread->id]);
        $response = $this->json('DELETE',$thread->path());
        $response->assertStatus(204);
        $this->assertDatabaseMissing('threads',['id' => $thread->id]);
        $this->assertDatabaseMissing('replies',$reply->only('id'));

        $this->assertEquals(0,Activity::count());

    }

    public function publishThread($overrides = []){
        $this->signIn();
        $thread = make('App\Models\Thread',$overrides);
        return $this->post('/threads',$thread->toArray());
    }
}
