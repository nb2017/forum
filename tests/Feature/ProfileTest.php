<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProfileTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_user_has_a_profile()
    {
        $user = create('App\Models\User');
        $this->withoutExceptionHandling()->get("/profiles/$user->name")
        ->assertSee($user->name);
    }

    public function test_profiles_display_all_threads_created_by_the_assosiated_user()
    {
        $this->signIn();
        $thread= create('App\Models\Thread',['user_id'=>auth()->id()]);
        $this->withoutExceptionHandling()->get("/profiles/".auth()->user()->name)
        ->assertSee($thread->title)
        ->assertSee($thread->body);
    }
}
