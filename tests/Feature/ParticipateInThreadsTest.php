<?php

namespace Tests\Feature;

use Tests\TestCase;

class ParticipateInThreadsTest extends TestCase
{
    


    public function test_unauthenticated_user_may_not_add_replies()
    {
        /*$this->expectException('Illuminate\Auth\AuthenticationException');

        $this->withoutExceptionHandling()->post('threads/channel/1/replies',[]);
        */
        $this->post('threads/channel/1/replies',[])->assertRedirect('/login');

    }


    public function test_an_authenticated_user_may_participate_in_forum_threads()
    {
        
        $this->signIn();
        $thread = create('App\Models\Thread');

        $reply = make('App\Models\Reply');

        $this->post($thread->path().'/replies',$reply->toArray());

        $this->get($thread->path())
        ->assertSee($reply->body);
    }

    public function test_a_reply_requires_a_body(){
        $this->signIn();

        $thread = create('App\Models\Thread');

        $reply = make('App\Models\Reply',['body' => null]);
        $this->post($thread->path().'/replies',$reply->toArray())
        ->assertSessionHasErrors('body');

        
    }

    public function test_authorized_users_cannot_delete_replies(){
    

        $reply = create('App\Models\Reply');

        $this->delete("/replies/{$reply->id}")->assertRedirect('/login');

        $this->signIn()
        ->delete("/replies/{$reply->id}")->assertStatus(403);
        
    }

    function test_authorized_users_can_delete_replies(){

        $this->signIn();
        $reply = create('App\Models\Reply',['user_id' => auth()->id()]);

        $this->delete("/replies/{$reply->id}")->assertStatus(302);

        $this->assertDatabaseMissing('replies',['id' => $reply->id]);
    }

    function test_authorized_users_can_update_replies(){

        $this->signIn();
        $reply = create('App\Models\Reply',['user_id' => auth()->id()]);
        $updatedReply = 'Changed Body';
        $this->patch("/replies/{$reply->id}",['body' => $updatedReply]);

        $this->assertDatabaseHas('replies',['id' => $reply->id , 'body' => $updatedReply]);
    }

    function test_unauthorized_users_cannot_update_replies(){

        
        $reply = create('App\Models\Reply');
  
        $this->patch("/replies/{$reply->id}")
        ->assertRedirect('/login');

        $this->signIn();
        $this->patch("/replies/{$reply->id}")
        ->assertStatus(403);
    }

}
