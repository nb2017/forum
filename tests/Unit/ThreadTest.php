<?php

namespace Tests\Unit;

use Tests\TestCase;

class ThreadTest extends TestCase
{
    

    public function setUp(){
    	parent::setUp();
    	$this->thread = create('App\Models\Thread');
    }

    public function test_a_thread_can_make_a_string()
    {
        $thread = create('App\Models\Thread');
        $this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}",$thread->path());
    }

    public function test_thread_has_creator()
    {
        $this->assertInstanceOf('App\Models\User',$this->thread->creator);
    }

    public function test_thread_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection',$this->thread->replies);
    }

    public function test_thread_can_add_a_reply()
    {
        $this->thread->addReply([
        	'body'		=>	'Foobar',
        	'user_id'	=> 	1
        ]);
        $this->assertCount(1,$this->thread->replies);
    }

    public function test_thread_belongs_to_a_channel()
    {
        
        /*$thread = create('App\Models\Thread');
        $this->assertInstanceOf('App\Models\Channel',$thread->channel);*/
        $this->assertInstanceOf('App\Models\Channel',$this->thread->channel);
    }
    
}
