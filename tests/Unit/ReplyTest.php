<?php

namespace Tests\Unit;

use Tests\TestCase;

class ReplyTest extends TestCase
{
    
    
    public function test_has_an_owner()
    {
        $reply = create('App\Models\Reply');
        $this->assertInstanceOf('App\Models\User',$reply->owner);
    }
}
