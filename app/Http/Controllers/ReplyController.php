<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use App\Models\Reply;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function store($channel,Thread $thread, Request $request)
    {
        $this->validate($request,[
            'body' =>  'required'
        ]);
        $thread->addReply([
        	'body'		=>	$request->body,
        	'user_id'	=>	auth()->id()
        ]);

        return back()->with('flash', 'Your reply has been left!');
    }

    public function destroy(Reply $reply){

        $this->authorize('update', $reply);

        $reply->delete();

        if(request()->wantsJson()){
            return response(['status'=>'Reply deleted']);
        }
        return back();
    }

    public function update(Reply $reply){

        $this->authorize('update', $reply);

        $reply->update(request(['body']));

        //return back();
    }
}
