<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use App\Models\Channel;
use App\Filters\ThreadFilters;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index','show']);
    }

    public function index(Channel $channel, ThreadFilters $filters)
    {

        $threads = $this->getThreads($channel,$filters);

        if(request()->wantsJson()){
            return $threads;
        }

        return view('threads.index',compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' =>  'required',
            'body' =>  'required',
            'channel_id' =>  'required|exists:channels,id'
        ]);
        $thread = Thread::create([
            'user_id'   =>  auth()->id(),
            'channel_id'     =>  $request->channel_id,
            'title'     =>  $request->title,
            'body'      =>  $request->body
        ]);
        return redirect($thread->path())->with('flash', 'Your reply has been left!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($channel,Thread $thread)
    {

        return view('threads.show',[
            'thread' => $thread,
            'replies' => $thread->replies()->paginate(20)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($channel,Thread $thread)
    {
        $this->authorize('update',$thread);
        /*if($thread->user_id != auth()->id()){
            abort(403, 'You do not have permission to do this.');
        }*/

        
        $thread->delete();
        if(request()->wantsJson()) return response([],204);
        return redirect('/threads');
    }

    protected function getThreads(Channel $channel,ThreadFilters $filters){
        
        $threads = Thread::latest()->filter($filters);

        if($channel->exists){
            $threads = $threads->where('channel_id',$channel->id);
        }
        //dd($threads->toSql());
        return  $threads->get();
    }
}
