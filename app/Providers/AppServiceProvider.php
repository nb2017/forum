<?php

namespace App\Providers;

use App\Models\Channel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        \View::composer('*',function($view){
            $view->with('channels',\App\Models\Channel::oldest('name')->get());
        });
        //\View::share('channels',Channel::oldest('name')->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
